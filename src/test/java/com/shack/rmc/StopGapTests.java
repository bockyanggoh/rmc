package com.shack.rmc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StopGapTests {

    @Autowired
    private RMCService rmcService;


    @Test
    public void rmcStopTest() {
        String stopGap = "12";
        StepVerifier.create(rmcService.stopGap(stopGap))
                .expectNext(1)
                .expectComplete()
                .verify();
    }

    @Test
    public void rmcGapTest(){
        String stopGap = "0";
        StepVerifier.create(rmcService.stopGap(stopGap))
                .expectNext(0)
                .expectComplete()
                .verify();
    }

    @Test
    public void rmcErrorTest(){
        StepVerifier.create(rmcService.stopGap(null))
                .verifyComplete();
    }
}
