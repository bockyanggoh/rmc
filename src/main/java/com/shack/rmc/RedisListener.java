package com.shack.rmc;

import com.shack.rmc.config.RedisQueuePicker;
import com.shack.rmc.service.CommandPattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
@EnableScheduling
public class RedisListener {

    @Autowired
    private RedisQueuePicker queues;

    @Autowired
    private ReactiveListOperations<String, String> listOps;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private String junkQueue;

    @Scheduled(fixedRate = 100L)
    public void listener(){
        Flux.fromIterable(queues.getQueues())
                .doOnNext(queue -> processQueue(queue))
                .parallel()
                .subscribe();
    }

    private void processQueue(RedisQueuePicker.RedisQueue queue){
        Mono.just(queue)
            .filter(q -> q.isEnabled())
            .flatMap(q -> listOps.leftPop(q.getName()))
            .doOnNext(msg -> {
                CommandPattern execute = (CommandPattern) context.getBean(queue.getBeanName());
                execute.process(msg);
            })
            .doOnError(error -> log.info("Error in queue: {}", error))
            .subscribe();
    }

    @Scheduled(fixedRate = 10L)
    public void junkListener(){
        listOps.leftPop(junkQueue)
                .subscribe(s -> log.info(s));
    }

}
