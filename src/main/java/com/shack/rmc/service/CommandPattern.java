package com.shack.rmc.service;

import com.shack.rmc.config.RedisQueuePicker;

import java.io.Serializable;

public interface CommandPattern extends Serializable {

    public void process(String message);

}
