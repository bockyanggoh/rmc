package com.shack.rmc.service;

import com.shack.rmc.config.RedisQueuePicker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class QueueTwoService implements CommandPattern {

    @Autowired
    private String junkQueue;

    @Autowired
    private ReactiveListOperations<String, String> redis;

    @Override
    public void process(String message) {
        redis.rightPush(junkQueue, message).subscribe();
    }
}
