package com.shack.rmc.config;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@ConfigurationProperties(prefix = "app.redis")
@Component
@Getter
@Setter
public class RedisQueuePicker {

    List<RedisQueue>  queues;

    @Getter
    @Setter
    public static class RedisQueue {
        private String name;
        private boolean enabled;
        private String beanName;

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
}

