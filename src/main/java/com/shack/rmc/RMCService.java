package com.shack.rmc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.validation.constraints.Null;

@Component
@Slf4j
public class RMCService {


    Mono<Integer> stopGap(String gap){
        return Mono.fromCallable(() -> {
            try {
                if(gap.length() > 1){
                    log.info("Stop!");
                    return 1;
                }
                else {
                    log.info("Gap!");
                    return 0;
                }
            }catch (NullPointerException e){
                log.info("Null Exception: {}");
                return null;
            }
        })
        .doOnError(error -> log.info("Error executing chain. {}", error))
        .doOnSuccess(data -> log.info("Process complete."));
    }
}
